import React from 'react';
import './css/App.css';
import Menu from './components/Menu/Menu';
import Main from './components/Main/Main';
import MenuContainer from "./components/Menu/MenuContainer";

const App = (props) => {
    return (
        <div className="app-wrapper">
            <MenuContainer />
            <Main />
        </div>
    );
}

export default App;
