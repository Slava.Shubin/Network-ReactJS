import React from 'react';
import style from './Activity.module.css';
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import Twitt from "./Twitt/Twitt";
import Cover from "../Cover/Cover";
import SideBlock from "../SideBlock/SideBlock";

const Activity = (props) => {
    let twittsElements = props.twitts.map(twitt => <Twitt id={twitt.id} title={twitt.title} time={twitt.time} text={twitt.text} comments={twitt.comments} />);

    // Создаём ссылку на элемент (текстовое поле)
    let newPost = React.createRef();

    // Add post
    let onAddPost = () => {
        props.onAddPost();
    }
    // Изменение в текстовом поле
    let onChangePost = () => {
        let text = newPost.current.value;
        props.updatePostText(text);
    }
    return(
        <div className={style.activity}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    {/* Activity content */}
                    <div className={style.activityContent}>
                        {/* Menu */}
                        <div className={style.activityMenu}>
                            <a href="#" className={style.link}>Personal</a>
                            <a href="#" className={style.link}>Mentions</a>
                            <a href="#" className={style.link}>Favorits</a>
                            <a href="#" className={style.link}>Friends</a>
                            <a href="#" className={style.link}>Groups</a>
                        </div>

                        {/* New twitt */}
                        <div className={style.newTwitt}>
                            <img src="images/5cbddac03094a-bpthumb.jpg" alt="Avatar" />
                            <div className={style.whatsNew}>
                                <p>
                                    What's new, Slava?
                                </p>
                                <textarea name="new" className={style.newArea} onChange={onChangePost} ref={newPost} value={props.newPostText} placeholder={"Что у вас нового?"}/>
                                <div className={style.buttons}>
                                    <i className="fa fa-music" aria-hidden="true"></i>
                                    <button className={style.post} onClick={onAddPost}>Post update</button>
                                </div>
                            </div>
                        </div>

                        {/* Twitts */}
                        <div className={style.twitts}>
                            {twittsElements}
                        </div>
                    </div>
                </div>
                <SideBlock/>
            </div>
        </div>
    );
}

export default Activity;