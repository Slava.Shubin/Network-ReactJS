import React from 'react';
import {addPostActionCreator, updatePostTextActionCreator} from "../../../redux/ActivityReducer";
import Activity from "./Activity";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
    return {
        twitts: state.activityPage.twitts,
        newPostText: state.activityPage.newPostText,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePostText: (text) => {
            dispatch(updatePostTextActionCreator(text));
        },
        onAddPost: () => {
            dispatch(addPostActionCreator());
        }
    }
}

// Create container-component
const ActivityContainer = connect(mapStateToProps, mapDispatchToProps)(Activity);

export default ActivityContainer;