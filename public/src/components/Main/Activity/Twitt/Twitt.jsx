import React from "react";
import style from "./Twitt.module.css";

const Twitt = (props) => {
    return (
        <div className={style.twitt}>
            <div className={style.head}>
                <img src="images/5cbddac03094a-bpthumb.jpg" alt="Avatar"/>
                <div className={style.title}>
                    <p><span className={style.orange}>{props.name}&nbsp;</span>{props.title}</p>
                    <p className={style.hours}>{props.time}</p>
                </div>
            </div>
            <div className={style.text}>
                {props.text}
            </div>
            <div className={style.buttons}>
                <i className="fa fa-comment-o" aria-hidden="true"> Comments {props.comments}</i>
                <i className="fa fa-trash-o" aria-hidden="true"> Delete</i>
            </div>
        </div>
    );
}

export default Twitt;