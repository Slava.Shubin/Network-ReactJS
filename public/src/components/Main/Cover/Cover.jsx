import React from "react";
import style from "./Cover.module.css";

const Cover = (props) => {
    return (
        <div className={style.wrapper}>
            <div className={style.cover}></div>
            {/* Block */}
            <div className={style.block}>
                <img src="images/5a100d3ace952-bpfull.jpg" alt="User photo"/>
                <div className={style.info}>
                    <p className={style.name}>{props.state.name}</p>
                    <p className={style.id}>{props.state.nickName} <span className={style.time}>{props.state.time}</span></p>
                    <p className={style.status}>{props.state.status}</p>
                </div>
            </div>
            <a href="" className={style.updateAvatar}>
                <i className="fa fa-user-o" aria-hidden="true"></i>
                Update Avatar
            </a>
            <a href="" className={style.updateCover}>
                <i className="fa fa-camera" aria-hidden="true"></i>
                Update Cover Image
            </a>
        </div>
    );
}

export default Cover;