import React from "react";
import style from './Events.module.css';
import Cover from "../Cover/Cover";
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import SideBlock from "../SideBlock/SideBlock";

const Events = (props) => {
    return (
        <div className={style.events}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    <div className={style.eventsContent}>
                        {/* Menu */}
                        <div className={style.activityMenu}>
                            <a href="#" className={style.link}>My profile</a>
                            <a href="#" className={style.link}>Events I'm Attending</a>
                            <a href="#" className={style.link}>My Events</a>
                            <a href="#" className={style.link}>My Locations</a>
                            <a href="#" className={style.link}>My Event Bookings</a>
                        </div>

                        <p className={style.title}>Events</p>
                        <p className={style.text}>
                            {props.text}
                        </p>
                    </div>
                </div>
                <SideBlock />
            </div>
        </div>
    );
}

export default Events;