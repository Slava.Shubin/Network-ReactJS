import React from 'react';
import style from './Footer.module.css';


const Footer = (props) => {
    return (
        <div className={style.footer}>
            {/* Footer Wrapper */}
            <div className={style.wrapper}>
                {/* Footer block */}
                <div className={style.block}>
                    <p className={style.title}>Get in touch</p>
                    <ul className={style.list}>
                        <li><a href="">Email</a></li>
                        <li><a href="">Help</a></li>
                        <li><a href="">FAQ'S</a></li>
                        <li><a href="">Knowledge base</a></li>
                    </ul>
                </div>
                {/* Footer block */}
                <div className={style.block}>
                    <p className={style.title}>Community</p>
                    <ul className={style.list}>
                        <li><a href="">Activity</a></li>
                        <li><a href="">Groups</a></li>
                        <li><a href="">Members</a></li>
                        <li><a href="">Forums</a></li>
                    </ul>
                </div>
                {/* Footer block */}
                <div className={style.block}>
                    <p className={style.title}>Our services</p>
                    <ul className={style.list}>
                        <li><a href="">Our mission</a></li>
                        <li><a href="">Help/Contact us</a></li>
                        <li><a href="">Privacy policy</a></li>
                        <li><a href="">Cookie policy</a></li>
                        <li><a href="">Terms &amp; conditions</a></li>
                    </ul>
                </div>
                {/* Footer block */}
                <div className={style.block}>
                    <p className={style.title}>Admin</p>
                    <ul className={style.list}>
                        <li><a href="">Advertising</a></li>
                        <li><a href="">Press</a></li>
                        <li><a href="">Affiliation</a></li>
                        <li><a href="">Business development</a></li>
                    </ul>
                </div>
            </div>

            {/* Footer Navigation */}
            <div className={style.navigation}>
                <div className={style.copyright}>
                    Copyright © 2020 <a href="">shubinshoter24@gmail.com</a>
                </div>
                <nav>
                    <a href="" className={style.link}>Home</a>
                    <a href="" className={style.link}>Blog</a>
                    <a href="" className={style.link}>Activity</a>
                    <a href="" className={style.link}>Members</a>
                    <a href="" className={style.link}>Groups</a>
                    <a href="" className={style.link}>Events</a>
                </nav>
            </div>
        </div>
    );
}

export default Footer;