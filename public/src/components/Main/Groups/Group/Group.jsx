import React from "react";
import style from './Group.module.css';

const Group = (props) => {
    return (
        <div className={style.group}>
            <div className={style.cover}>
                <div className={style.members}>
                    <i className="fa fa-user-o" aria-hidden="true"></i>&nbsp;
                    {props.members}
                </div>
                <a href="#">
                    <img src={props.pathCover} alt="Cover image for group"/>
                </a>
                <a href="#">
                    <img className={style.logo} src={props.pathLogo} alt="Group's avatar"/>
                    <div className={style.overlay}><span>+</span></div>
                </a>
            </div>

            <p className={style.name}>{props.name}</p>
            <span className={style.time}>{props.time}</span>
            <p className={style.text}>
                {props.text}
            </p>
            <div className={style.info}>
                <span>{props.status}</span>
                <a href="#" className={style.leave}>Leave group</a>
            </div>
        </div>
    );
}

export default Group;