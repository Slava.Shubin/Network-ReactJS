import React from "react";
import style from './Groups.module.css';
import Cover from "../Cover/Cover";
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import SideBlock from "../SideBlock/SideBlock";
import Group from "./Group/Group";

const Groups = (props) => {
    let groupsElement = props.groups.map(group => <Group members={group.members} pathCover={group.pathCover} pathLogo={group.pathLogo} name={group.name} time={group.time} text={group.text} status={group.status + " group"} />);

    return(
        <div className={style.groups}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    <div className={style.groupsContent}>
                        {/* Menu */}
                        <div className={style.activityMenu}>
                            <a href="#" className={style.link}>My profile</a>
                            <a href="#" className={style.link}>Events I'm Attending</a>
                            <a href="#" className={style.link}>My Events</a>
                            <a href="#" className={style.link}>My Locations</a>
                            <a href="#" className={style.link}>My Event Bookings</a>
                        </div>

                        {/* Numeration */}
                        <div className={style.numeration}>
                            <div className={style.numberGroups}>
                                Viewing 1 - 8 of 8 groups
                            </div>
                            <div className={style.numberPages}>
                                <a href="#">1</a>
                                <a href="#">2</a>
                                <a href="#">
                                    <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                        {/* Groups */}
                        <div className={style.groupWrapper}>
                            {groupsElement}
                        </div>

                        {/* Numeration */}
                        <div className={style.numeration}>
                            <div className={style.numberGroups}>
                                Viewing 1 - 8 of 8 groups
                            </div>
                            <div className={style.numberPages}>
                                <a href="#">1</a>
                                <a href="#">2</a>
                                <a href="#">
                                    <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <SideBlock />
            </div>
        </div>
    );
}

export default Groups;