import React from "react";
import {connect} from "react-redux";
import Groups from "./Groups";

const mapStateToProps = (state) => {
    return {
        groups: state.groupsPage.groups,
        user: state.user
    }
}

let groupsContainer = connect(mapStateToProps)(Groups);

export default groupsContainer;