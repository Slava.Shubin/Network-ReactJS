import React from 'react';
import style from './Header.module.css';
import {Link} from "react-router-dom";


const Header = (props) => {
    return (
        <header className={style.header}>
            {/* Navbar */}
            <div className={style.navbar}>
                <nav className={style.navigation}>
                    <Link to="/home" className={style.navLink}>Home</Link>
                    <a className={style.blogLink}>
                        Blog <i className="fa fa-angle-down" aria-hidden="true"></i>
                        <div className={style.blogPopap}>
                            {/* Post */}
                            <div className={style.post}>
                                <a href="#"><img src="images/elephant-1822636_1280-3-250x135.jpg" alt="Post photo"/></a>
                                <a className={style.blogTitle}>
                                    Objectively incubate strategic growth
                                </a>
                                <div className={style.date}>
                                    November 28, 2017
                                </div>
                            </div>
                            {/* Post */}
                            <div className={style.post}>
                                <a href="#"><img src="images/photodune-2359890-elegant-hairstyle-s-3-250x135.jpg"
                                                 alt="Post photo"/></a>
                                <a className={style.blogTitle}>
                                    Completely stratgize client-based niche
                                </a>
                                <div className={style.date}>
                                    November 28, 2017
                                </div>
                            </div>
                            {/* Post */}
                            <div className={style.post}>
                                <a href="#"><img src="images/mystery-1599527_1920-3-250x135.jpg" alt="Post photo"/></a>
                                <a className={style.blogTitle}>
                                    Intrinsicly streamline go forward internal
                                </a>
                                <div className={style.date}>
                                    November 28, 2017
                                </div>
                            </div>
                            {/* Post */}
                            <div className={style.post}>
                                <a href="#"><img src="images/green-hair-women-250x135.jpg" alt="Post photo"/></a>
                                <a className={style.blogTitle}>
                                    Enthusiastically revolutionize backend
                                </a>
                                <div className={style.date}>
                                    November 18, 2017
                                </div>
                            </div>
                        </div>
                    </a>
                    <a className={style.navLink} href="#">Activity</a>
                    <a className={style.navLink} href="#">Members</a>
                    <a className={style.navLink} href="#">Groups</a>
                    <a className={style.navLink} href="#">Events</a>
                </nav>
                <div className={style.searchBlock}>
                    <i className="fa fa-search" aria-hidden="true" id={style.searchIcon}></i>
                    <a href="#" className={style.profileLogo} id={style.profileLogo}>
                        <img src="images/5cbddac03094a-bpthumb.jpg" alt="LinkContainer logo"/>
                        <div className={style.profileInfo}>
                            <a href="#" className={style.profileInfo__link}>
                                LinkContainer
                            </a>
                            <a href="#" className={style.profileInfo__link}>
                                Logout
                            </a>
                        </div>
                    </a>
                </div>
                <input type="search" id={style.search} placeholder="Search"/>
            </div>
        </header>
    );
}

export default Header;