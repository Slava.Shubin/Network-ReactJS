import React from 'react';
import style from './Comments.module.css';

const Comments = (props) => {
    return (
        <div className="comments sectionBlock">
            <p className="sectionBlock__title">Recent comments</p>
            {/* CommentsLink */}
            <div className={style.link}>
                Demo on <a href="#">Enthusiastically revolutionize backend</a>
            </div>

            {/* CommentsLink */}
            <div className={style.link}>
                <a href="#">Türk</a> on <a href="#">Objectively incubate strategic growth</a>
            </div>

            {/* CommentsLink */}
            <div className={style.link}>
                Maria Kim on <a href="#">Enthusiastically revolutionize backend</a>
            </div>

            {/* CommentsLink */}
            <div className={style.link}>
                Billy Howard on <a href="#">Objectively incubate strategic growth</a>
            </div>

            {/* CommentsLink */}
            <div className={style.link}>
                Albert Moreno on <a href="#">Conveniently provide access to professional methods</a>
            </div>
        </div>
    );
}

export default Comments;