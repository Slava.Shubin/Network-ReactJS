import React from 'react';
import Event from './Event/Event';

const Events = (props) => {
    return (
        <div className="events sectionBlock">
            <p className="sectionBlock__title">Events</p>
            {/* EventsLink */}
            <Event title="Grand design live London" date="22/02/2020 - 28/02/2030" info="1 Western Gateway"/>
            
            {/* EventsLink */}
            <Event title="Comic-con" date="27/02/2020 - 02/03/2030" info="San Diego"/>
            
            {/* EventsLink */}
            <Event title="Camden town brewery" date="18/03/2020 - 29/03/2030" info="London"/>
        </div>
    );
}

export default Events;