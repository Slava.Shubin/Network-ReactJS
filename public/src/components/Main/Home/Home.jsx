import React from 'react';
import style from './Home.module.css';
import MyGroups from './MyGroups/MyGroups';
import Groups from './Groups/Groups';
import Members from './Members/Members';
import Events from './Events/Events';
import Posts from './Posts/Posts';
import Comments from './Comments/Comments';
import Statistics from './Statistics/Statistics';


const Home = () => {
    return (
        <div className={style.home}>
            <p className="sectionTitle">
                Dashboard
            </p>
            <div className={style.homeBlock}>
                {/* Welcome */}
                <div className={style.welcome}>
                    <b><p className="sectionBlock__title">Welcome</p></b>
                    <div className={style.welcome__text}>Network can be used for any business to create an intranet/extranet
                        or private network. Full integrated with BuddyPress, bbPress and Events
                        Manager.
                    </div>
                </div>

                {/* Events SectionBlock */}
                <Events />

                {/* Recent Posts */}
                <Posts />

                {/* My Groups SectionBlock */}
                <MyGroups />

                {/* Groups SectionBlick */}
                <Groups />

                {/* Recent Comments SectionBlock */}
                <Comments />

                {/* Members SectionBlock */}
                <Members />

                {/* Statistics */}
                <Statistics />

            </div>
        </div>
    );
}

export default Home;