import React from 'react';
import style from './Members.module.css';
import User from './User/User';

const Members = (props) => {
    return (
        <div className={`${style.members} sectionBlock`}>
            <p className="sectionBlock__title">Members</p>
            {/* MembersLink */}
            <User path="images/5a100d3ad2b15-bpthumb.jpg" name="billyhoward" info="26 friends"/>

            {/* MembersLink */}
            <User path="images/5a0f4a768de5f-bpthumb.jpg" name="Rebeca Carter" info="25 friends"/>

            {/* MembersLink */}
            <User path="images/5a0f3a30b9c0c-bpthumb.jpg" name="albertmoreno" info="25 friends"/>

            {/* MembersLink */}
            <User path="images/5a0f4a0b706f7-bpthumb.jpg" name="terripeters" info="24 friends"/>

            {/* MembersLink */}
            <User path="images/5a0f4ca1a45d7-bpthumb.jpg" name="Jerry Ward" info="22 friends"/>
        </div>
    );
}

export default Members;