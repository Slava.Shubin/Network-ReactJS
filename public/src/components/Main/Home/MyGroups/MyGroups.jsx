import React from 'react';
import style from './MyGroups.module.css';
import Group from './Group/Group';


const MyGroups = (props) => {
    return (
        <div className={`${style.myGroups} sectionBlock`}>
            <p className="sectionBlock__title">My Groups</p>
            {/* GroupLink */ }
            <Group path="images/mystery-group.png" name="Back to the Future" info="active 4 days, 4 hours ago"/>

            {/* GroupLink */ }
            <Group path="images/mystery-group.png" name="Black Swan" info="active 1 week, 5 days ago"/>

            {/* GroupLink */ }
            <Group path="images/5a101eab2bc2d-bpfull.jpg" name="Calligraphy" info="active 1 week, 6 days ago"/>

            {/* GroupLink */ }
            <Group path="images/5a101a45eca46-bpfull.jpg" name="Cricket Professionals" info="active 5 days, 12 hours ago"/>

            {/* GroupLink */ }
            <Group path="images/5a101aea813c2-bpfull.jpg" name="Dollhouses" info="active 1 month, 3 weeks ago"/>
        </div>
    );
}

export default MyGroups;