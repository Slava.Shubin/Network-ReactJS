import React from 'react';
import Post from './Post/Post';

const Posts = () => {
    return (
        <div className="posts sectionBlock">
            <p className="sectionBlock__title">Posts</p>
            {/* Recent Post */}
            <Post path="images/elephant-1822636_1280-3-75x75.jpg" title="Objectively incubate strategic growth" date="November 28, 2017"/>

            {/* Recent Post */}
            <Post path="images/photodune-2359890-elegant-hairstyle-s-3-75x75.jpg" title="Completely strategize client-based niche" date="November 28, 2017"/>

            {/* Recent Post */}
            <Post path="images/mystery-1599527_1920-3-75x75.jpg" title="Intrinsicly streamline go forward internal" date="November 28, 2017"/>

            {/* Recent Post */}
            <Post path="images/green-hair-women-75x75.jpg" title="Enthusiastically revolutionize backend" date="November 18, 2017"/>

            {/* Recent Post */}
            <Post path="images/5a101a45eca46-bpfull.jpg" title="Conveniently provide access to professional methods" date="November 18, 2017"/>
        </div>
    );
}

export default Posts;