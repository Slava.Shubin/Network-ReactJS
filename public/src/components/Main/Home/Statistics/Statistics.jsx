import React from 'react';
import Block from './Block/Block';

const Statistics = (props) => {
    return (
        <div className="statistics sectionBlock">
            <p className="sectionBlock__title">Statistics</p>
            {/* Statistics Block */}
            <Block class="fa fa-file-text-o" text="Posts" number="3,511"/>

            {/* Statistics Block */}
            <Block class="fa fa-comment-o" text="Comments" number="9,889"/>

            {/* Statistics Block */}
            <Block class="fa fa-commenting-o" text="Activity" number="59,302"/>

            {/* Statistics Block */}
            <Block class="fa fa-user-o" text="Members" number="19,489"/>

            {/* Statistics Block */}
            <Block class="fa fa-user-circle-o" text="Online" number="2,538"/>

            {/* Statistics Block */}
            <Block class="fa fa-users" text="Groups" number="1,692"/>
            
            {/* Statistics Block */}
            <Block class="fa fa-list" text="Forums" number="1,124"/>
            
            {/* Statistics Block */}
            <Block class="fa fa-comments" text="Topics" number="23,473"/>
        </div>
    );
}

export default Statistics;