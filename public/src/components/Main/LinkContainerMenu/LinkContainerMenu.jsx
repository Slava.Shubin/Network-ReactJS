import React from 'react';
import style from './LinkContainerMenu.module.css';

const LinkContainerMenu = (props) => {
    return (
        <div className={style.menu}>
            <a href="#"><i className="fa fa-list" aria-hidden="true"></i><span>Activity</span></a>
            <a href="#"><i className="fa fa-user-o" aria-hidden="true"></i><span>Profile</span></a>
            <a href="#"><i className="fa fa-bell-o" aria-hidden="true"></i><span>Notifications</span></a>
            <a href="#"><i className="fa fa-envelope-o" aria-hidden="true"></i><span>Messages</span></a>
            <a href="#"><i className="fa fa-pencil-square-o" aria-hidden="true"></i><span>Posts</span></a>
            <a href="#"><i className="fa fa-user-plus" aria-hidden="true"></i><span>Friends</span></a>
            <a href="#"><i className="fa fa-users" aria-hidden="true"></i><span>Groups</span></a>
            <a href="#"><i className="fa fa-calendar" aria-hidden="true"></i><span>Events</span></a>
            <a href="#"><i className="fa fa-video-camera" aria-hidden="true"></i><span>Media</span></a>
            <a href="#"><i className="fa fa-cog" aria-hidden="true"></i><span>Settings</span></a>
        </div>
    );
}

export default LinkContainerMenu;