import React from 'react';
import Header from './Header/Header';
import Home from './Home/Home';
import Footer from './Footer/Footer';
import {Route, Switch} from "react-router-dom";
import ActivityContainer from "./Activity/ActivityContainer";
import MessagesContainer from "./Messages/MessagesContainer";
import ProfileContainer from "./Profile/ProfileContainer";
import NotificationsContainer from "./Notifications/NotificationsContainer";
import EventsContainer from "./Events/EventsContainer";
import GroupsContainer from "./Groups/GroupsContainer";
import SettingsContainer from "./Settings/SettingsContainer";

const Main = (props) => {
    return (
        <div className="main">
            <Header/>
            <div className="mainContent">
                <Switch>
                    <Route exact path='/' render={() => <Home />} />
                    <Route path='/home' render={() => <Home />} />
                    <Route path='/activity' render={() => <ActivityContainer
                        store={props.store} />} />
                    <Route path='/profile' render={() => <ProfileContainer
                        state={props.state} />} />
                    <Route path='/messages' render={() => <MessagesContainer
                        store={props.store} />} />
                    <Route path='/notifications' render={() => <NotificationsContainer
                        store={props.store} />} />
                    <Route path='/events' render={() => <EventsContainer
                        store={props.store} />} />
                    <Route path='/groups' render={() => <GroupsContainer
                        state={props.state} />} />
                    <Route path='/settings' render={() => <SettingsContainer
                        state={props.state} />} />
                </Switch>
            </div>
            <Footer/>
        </div>
    );
}

export default Main;