import React from "react";
import style from './Messages.module.css';
import Cover from "../Cover/Cover";
import Dialog from "./Dialog/Dialog";
import Message from "./Message/Message";

const Messages = (props) => {
    let dialogsElement = props.dialogs.map(dialog => <Dialog id={dialog.id} path={"images/" + dialog.path} name={dialog.name} time={dialog.time}/>);
    let messagesElements = props.messages.map(message => <Message text={message.text} pathAvatar={message.pathAvatar} name={message.name} time={message.time} />);

    // Создаём ссылку на элемент
    let message = React.createRef();

    // Send message
    let onSendMessage = () => {
        props.sendMessage();
    }
    // Изменение в тестовом поле
    let onChangeMessage = () => {
        let text = message.current.value;
        props.updateMessageText(text);
    }

    return (
        <div id={style.messages}>
            <Cover state={props.user}/>
            <div className={style.wrapper}>
                {/* Dialogs */}
                <div className={style.dialogs}>
                    <p className={style.title}>Dialogs</p>
                    {/* Dialog */}
                    {dialogsElement}
                </div>

                {/* Messages */}
                <div className={style.messages}>
                    {messagesElements}

                    {/* Input message */}
                    <div className={style.input}>
                        <textarea name="input" ref={message} onChange={onChangeMessage} value={props.newMessageText} placeholder={"Ваше сообщение"}/>
                        <button className={style.send} onClick={onSendMessage}>Send</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Messages;