import React from "react";
import style from './Notifications.module.css';
import Cover from "../Cover/Cover";
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import SideBlock from "../SideBlock/SideBlock";

const Notifications = (props) => {
    return (
        <div className={style.notifications}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    <div className={style.notificationsContent}>
                        {/* Menu */}
                        <div className={style.activityMenu}>
                            <a href="#" className={style.link}>Unread</a>
                            <a href="#" className={style.link}>Read</a>
                        </div>

                        <div className={style.noNotif}>
                            You have not unread notification.
                        </div>
                    </div>
                </div>
                <SideBlock />
            </div>
        </div>
    );
}

export default Notifications;