import React from 'react';
import style from './Profile.module.css';
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import Cover from "../Cover/Cover";
import SideBlock from "../SideBlock/SideBlock";

const Profile = (props) => {
    return (
        <div className={style.profile}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    {/* Profile Content */}
                    <div className={style.profileContent}>
                        <div className={style.profileMenu}>
                            <a href="#" className={style.link}>View</a>
                            <a href="#" className={style.link}>Edit</a>
                            <a href="#" className={style.link}>Change profile photo</a>
                            <a href="#" className={style.link}>Change cover image</a>
                        </div>

                        {/* Profile Info */}
                        <div className={style.profileInfo}>
                            <p className={style.title}>Profile info</p>
                            <table className={style.keys}>
                                <tr className={style.key}>
                                    <td>Nickname</td>
                                    <td>Slava</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Age</td>
                                    <td>20</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>I am</td>
                                    <td>Programmer</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>First name</td>
                                    <td>Slava</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Last name</td>
                                    <td>Shubin</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Seeking</td>
                                    <td>Success!</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Country</td>
                                    <td>Russia</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Looking for</td>
                                    <td>Relationship</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>City</td>
                                    <td>Krasnodar</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Interests</td>
                                    <td>WEB, ReactJS, JavaScript, HTML, CSS, PHP, MySQL</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Biographical</td>
                                    <td>I am a student</td>
                                </tr>
                            </table>
                        </div>

                        {/* Profile Info */}
                        <div className={style.profileInfo}>
                            <p className={style.title}>Single Fields</p>
                            <table className={style.keys}>
                                <tr className={style.key}>
                                    <td>URL</td>
                                    <td><a href="">
                                        http://www.clubedomalte.com.br</a></td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Textbox</td>
                                    <td>CDM</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Number</td>
                                    <td>+7 (900) 123-45-67</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Textarea</td>
                                    <td>We are the champions!</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Date of birthday</td>
                                    <td>2000-04-28</td>
                                </tr>
                            </table>
                        </div>

                        {/* Profile Info */}
                        <div className={style.profileInfo}>
                            <p className={style.title}>Developer Info</p>
                            <table className={style.keys}>
                                <tr className={style.key}>
                                    <td>Skills</td>
                                    <td>ReactJS</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Country</td>
                                    <td>Russia</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Website</td>
                                    <td><a href="">http://www.clubedomalte.com.br</a></td>
                                </tr>
                            </table>
                        </div>

                        {/* Profile Info */}
                        <div className={style.profileInfo}>
                            <p className={style.title}>Contacts</p>
                            <table className={style.keys}>
                                <tr className={style.key}>
                                    <td>E-mail</td>
                                    <td><a href="mailto:shubinshoter24@gmail.com">shubinshoter24@gmail.com</a></td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Phoone</td>
                                    <td>+7 (900) 123-45-67</td>
                                </tr>
                                <tr className={style.key}>
                                    <td>Address</td>
                                    <td>Russia, Krasnodar, Sormovskaya street 204/7</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <SideBlock />
            </div>
        </div>
    );
}

export default Profile;