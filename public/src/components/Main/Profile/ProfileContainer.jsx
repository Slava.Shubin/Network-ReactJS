import React from "react";
import {connect} from "react-redux";
import Profile from "./Profile";

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

let profileContainer = connect(mapStateToProps)(Profile);

export default profileContainer;