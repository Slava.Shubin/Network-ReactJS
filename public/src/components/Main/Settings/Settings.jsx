import React from "react";
import style from './Settings.module.css';
import Cover from "../Cover/Cover";
import LinkContainerMenu from "../LinkContainerMenu/LinkContainerMenu";
import SideBlock from "../SideBlock/SideBlock";

const Settings = (props) => {
    return (
        <div className={style.settings}>
            <Cover state={props.user}/>
            <div className={style.content}>
                <div className={style.contentWrapper}>
                    <LinkContainerMenu />

                    <div className={style.settingsContent}>
                        {/* Menu */}
                        <div className={style.activityMenu}>
                            <a href="#" className={style.link}>General</a>
                            <a href="#" className={style.link}>Email</a>
                            <a href="#" className={style.link}>Profile Visability</a>
                            <a href="#" className={style.link}>Export Data</a>
                        </div>

                        {/* Password */}
                        <div className={style.password}>
                            <p>Current Password (required to update email or change current password)</p>
                            <input type="password" />
                            <span>Lost your password?</span>
                        </div>

                        {/* Email */}
                        <div className={style.email}>
                            <p>Account email</p>
                            <input type="email" placeholder={"Email"} />
                        </div>

                        {/* Change */}
                        <div className={style.change}>
                            <p>Change Password (leave blank for no change)</p>
                            <input type="password" />
                            <span>New password</span>
                            <br/>
                            <input type="password" />
                            <span>Repeat new password</span>
                        </div>

                        <button className={style.save}>Save Changes</button>
                    </div>
                </div>
                <SideBlock />
            </div>
        </div>
    );
}

export default Settings;