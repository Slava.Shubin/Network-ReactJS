import React from 'react';
import style from './Menu.module.css';
import {NavLink} from "react-router-dom";
import UserLink from "./UserLink/UserLink";

const Menu = (props) => {
    let usersElements = props.users.map(user => <UserLink pathAvatar={user.pathAvatar} name={user.name} friends={user.friends} />);
    let activeUsersElements = props.activeUsers.map(user => <a href="#"><img className={style.activeMembers__avatar} src={user.pathAvatar} alt="Active members avatar"/></a>);
    return (
        <div className={`${style.menu} menu`}>
            {/* Logo */}
            <div className={style.logo}>
                <a href="#"><img src="images/logo.png" alt="Logo"/></a>
            </div>
            {/* Menu bar */}
            <nav className={style.menuBar}>
                <NavLink activeClassName={style.activeLink} to="/activity" className={style.menuBar__link}>
                    <i class="fa fa-list" aria-hidden="true"></i>Activity
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/profile" className={style.menuBar__link}>
                    <i class="fa fa-user" aria-hidden="true"></i>Profile
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/messages" className={style.menuBar__link}>
                    <i className="fa fa-envelope" aria-hidden="true"></i>Messages
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/notifications" className={style.menuBar__link}>
                    <i class="fa fa-bell" aria-hidden="true"></i>Notifications
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/events" className={style.menuBar__link}>
                    <i class="fa fa-calendar" aria-hidden="true"></i>Events
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/groups" className={style.menuBar__link}>
                    <i class="fa fa-users" aria-hidden="true"></i>Groups
                </NavLink>
                <NavLink activeClassName={style.activeLink} to="/settings" className={style.menuBar__link}>
                    <i class="fa fa-cog" aria-hidden="true"></i>Settings
                </NavLink>
            </nav>
            {/* Members */}
            <div className={style.members}>
                <p className={style.menuSectionTitle}>Members</p>
                <div className="membersActive">
                    <NavLink activeClassName="activeMembersLink" to="/newestMembers"
                          className="membersActive__link">Newest</NavLink>
                    <NavLink activeClassName="activeMembersLink" to="/activeMembers"
                          className="membersActive__link">Active</NavLink>
                    <NavLink activeClassName="activeMembersLink" to="/popularMembers"
                          className="membersActive__link">Popular</NavLink>
                </div>
                {usersElements}
            </div>
            {/* Recently active members */}
            <div className={style.activeMembers}>
                <p className={style.menuSectionTitle}>Recently active members</p>
                <div className={style.activeMembers__block}>
                    {activeUsersElements}
                </div>
            </div>
            {/* About us */}
            <div className={style.about}>
                <p className={style.menuSectionTitle}>About us</p>
                <p className={style.aboutText}>
                    {/*{props.info.aboutText}*/}
                </p>
                <div className={style.email}>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    {/*{props.info.email}*/}
                </div>
                <div className={style.phone}>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    {/*{props.info.phone}*/}
                </div>
            </div>
            {/* Bottom panel */}
            <div className={style.bottomPanel}><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></div>
        </div>
    );
}

export default Menu;