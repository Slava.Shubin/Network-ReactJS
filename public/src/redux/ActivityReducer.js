const ADD_POST = 'ADD-POST';
const UPDATE_POST_TEXT = 'UPDATE-POST-TEXT'

// Стартовый объект (по умолчанию)
let initialState = {
    twitts: [
        {
            id: 1,
            name: "Slava Shubin",
            title: "posted an update",
            time: "7 july",
            text: "Hello, World!",
            comments: 0
        },
        {
            id: 2,
            name: "Slava Shubin",
            title: "posted an update in the group Jazz",
            time: "6 july",
            text: "Всем привет!",
            comments: 1
        },
        {
            id: 3,
            name: "Slava Shubin",
            title: "posted an update",
            time: "5 july",
            text: "Занимаюсь вебом",
            comments: 0
        },
        {
            id: 4,
            name: "Slava Shubin",
            title: "replied to the topic deneme in the forum Forum 1",
            time: "3 july",
            text: "Крутая социальная сеть!",
            comments: 2
        },
        {
            id: 5,
            name: "Slava Shubin",
            title: "posted an update",
            time: "30 june",
            text: "Мой первый пост",
            comments: 0
        },
    ],
    newPostText: ''
}

const activityReducer = (state = initialState, action) => {
    // Добавление поста
    switch (action.type) {
        case ADD_POST: {
            let newPost = {
                id: 6,
                name: state.nickName,
                title: "posted an update",
                time: "1 minute ago",
                text: state.newPostText,
                comments: 0
            };
            return {
                ...state,
                twitts: [...state.twitts, newPost],
                newPostText: ''
            };
            // stateCopy.twitts = [...state.twitts];
            // stateCopy.twitts.push(newPost);
            // stateCopy.newPostText = '';
            // return stateCopy;
        }
        case UPDATE_POST_TEXT: {
            return {
                ...state,
                newPostText: action.newText
            };
            // stateCopy.newPostText = action.newText;
            // return stateCopy;
        }
        default:
            return state;
    }
}

export const addPostActionCreator = () => ({
    type: ADD_POST
});

export const updatePostTextActionCreator = (text) => ({
    type: UPDATE_POST_TEXT,
    newText: text
});

export default activityReducer;