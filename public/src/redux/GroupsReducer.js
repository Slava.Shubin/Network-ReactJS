let initialState = {
    groups: [
        {
            id: 1,
            members: 24,
            pathCover: "images/5a101e837705b-bp-cover-image.jpg",
            pathLogo: "images/5a101e7b809a3-bpthumb.jpg",
            name: "Drawing Lovers",
            time: "17 hours ago",
            text: "Competently formulate enterprise-wide synergy before 24/7 quality vectors. Holisticly implement enterprise intellectual capital through enabled expertise. Enthusiastically exploit.",
            status: "Public"
        },
        {
            id: 2,
            members: 12,
            pathCover: "images/5a101e3ee513a-bp-cover-image.jpg",
            pathLogo: "images/5a101deef28bc-bpthumb.jpg",
            name: "Jazz",
            time: "18 hours ago",
            text: "Dynamically repurpose B2B platforms after leading-edge infrastructures. Globally extend principle-centered internal or ”organic” sources for long-term high-impact value. Conveniently strategize real-time schemas through.",
            status: "Public"
        },
        {
            id: 3,
            members: 21,
            pathCover: "images/WhiteBG.png",
            pathLogo: "images/5c86f99ad5bf0-bpfull.jpg",
            name: "Huckers",
            time: "3 month ago",
            text: "Huckers",
            status: "Private"
        },
        {
            id: 4,
            members: 51,
            pathCover: "images/5a101b23b8081-bp-cover-image.jpg",
            pathLogo: "images/5a101b3a12e97-bpfull.jpg",
            name: "Drawing Association",
            time: "4 hours ago",
            text: "Enthusiastically re-engineer B2C human capital after transparent paradigms.",
            status: "Public"
        },
        {
            id: 5,
            members: 38,
            pathCover: "images/5a101ba65fd0d-bp-cover-image.jpg",
            pathLogo: "images/5a101bcb3fed9-bpfull.jpg",
            name: "Food Gang",
            time: "1 day ago",
            text: "Conveniently harness team driven customer service for cost effective core.",
            status: "Public"
        },
        {
            id: 6,
            members: 11,
            pathCover: "images/5a101a50d2bcb-bp-cover-image.jpg",
            pathLogo: "images/5a101a45eca46-bpfull.jpg",
            name: "Cricket Professionals",
            time: "3 hours ago",
            text: "Globally incubate worldwide infrastructures before real-time metrics. Objectively integrate top-line deliverables.",
            status: "Public"
        },
        {
            id: 7,
            members: 8,
            pathCover: "images/5a101d3b9ba1f-bp-cover-image.jpg",
            pathLogo: "images/5a101d351d90a-bpfull.jpg",
            name: "Sport Group",
            time: "3 hours ago",
            text: "Dramatically enhance wireless leadership rather than turnkey systems. Appropriately seize one-to-one web services through.",
            status: "Public"
        },
        {
            id: 8,
            members: 70,
            pathCover: "images/5a101e97b0432-bp-cover-image.jpg",
            pathLogo: "images/5a101eab2bc2d-bpfull.jpg",
            name: "Calligraphy",
            time: "20 hours ago",
            text: "Energistically procrastinate stand-alone value via front-end initiatives. Dramatically engage high standards in value before plug-and-play methodologies. Progressively utilize.",
            status: "Public"
        },
    ]
}

const groupsReducer = (state = initialState, action) => {
    return state;
}

export default groupsReducer;