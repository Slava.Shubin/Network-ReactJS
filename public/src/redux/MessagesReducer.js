const SEND_MESSAGE = 'SEND-MESSAGE';
const UPDATE_MESSAGE_TEXT = 'UPDATE-MESSAGE-TEXT';

// Стартовый объект (по умолчанию)
let initialState = {
    dialogs: [
        {id: 1, path: "5a100d3ad2b15-bpthumb.jpg", name: "billyhoward", time: "2 hours ago"},
        {id: 2, path: "5a0f4a768de5f-bpthumb.jpg", name: "Rebeca Carter", time: "online"},
        {id: 3, path: "5a0f3a30b9c0c-bpthumb.jpg", name: "Albert Moreno", time: "31 minutes ago"},
        {id: 4, path: "5a0f4a0b706f7-bpthumb.jpg", name: "terripeters", time: "54 minutes ago"},
        {id: 5, path: "5a0f4ca1a45d7-bpthumb.jpg", name: "Jeffery Ward", time: "31 minutes ago"},
        {id: 6, path: "5a0f495887a17-bpthumb.jpg", name: "Robert Nelson", time: "online"},
        {id: 7, path: "5cbddac03094a-bpthumb.jpg", name: "Meu Ovo", time: "online"},
        {id: 8, path: "5a0f497a00fb5-bpthumb.jpg", name: "wpengine", time: "3 hours ago"},
        {id: 9, path: "29dfc19f0f889f9a43e44e733fcfda08.png", name: "Wynonna Judd", time: "7 hours ago"},
        {id: 10, path: "5a10090f0a236-bpthumb.jpg", name: "Judith Holmes", time: "1 hours ago"},
        {id: 11, path: "5a0f4ae20a6bd-bpthumb.jpg", name: "Kathryn Wallace", time: "online"},
        {id: 12, path: "5a0f4ccfc8d10-bpthumb.jpg", name: "Frank Robertson", time: "online"},
        {id: 13, path: "5a1007cd676c0-bpthumb.jpg", name: "Denis Bishop", time: "online"},
        {id: 14, path: "5a100a06af155-bpthumb.jpg", name: "Dale Sims", time: "5 hours ago"},
    ],
    messages: [
        {
            id: 4,
            pathAvatar: "images/5a100d3ad2b15-bpthumb.jpg",
            name: "Slava",
            time: "21:10",
            text: "I'm fine too! It's my first post!"
        },
        {
            id: 3,
            pathAvatar: "images/5a0f495887a17-bpthumb.jpg",
            name: "Robert",
            time: "21:07",
            text: "Hello, Slava! A'm fine! And You?"
        },
        {
            id: 2,
            pathAvatar: "images/5a100d3ad2b15-bpthumb.jpg",
            name: "Slava",
            time: "21:06",
            text: "How are you?"
        },
        {id: 1, pathAvatar: "images/5a100d3ad2b15-bpthumb.jpg", name: "Slava", time: "21:04", text: "Hello!"},
    ],
    // Значение по умолчанию в строке ввода
    newMessageText: ''
}

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        // Отправка сообщения
        case SEND_MESSAGE: {
            let newMessage = {
                id: 5,
                pathAvatar: "images/5a0f495887a17-bpthumb.jpg",
                name: "Robert",
                time: "16:24",
                text: state.newMessageText
            }
            return {
                ...state,
                messages: [...state.messages, newMessage],
                newMessageText: ''
            }
            // let stateCopy = {...state};
            // stateCopy.messages = [...state.messages];
            // stateCopy.messages.push(newMessage);
            // stateCopy.newMessageText = '';
            // return stateCopy;
        }
        // Изменение state при написании сообщения
        case UPDATE_MESSAGE_TEXT: {
            return {
                ...state,
                newMessageText: action.newText
            };
            // stateCopy.newMessageText = action.newText;
            // return stateCopy;
        }
        default:
            return state;
    }
}

export const sendMessageActionCreator = () => ({
    type: SEND_MESSAGE
});

export const onChangeMessageActionCreator = (text) => ({
    type: UPDATE_MESSAGE_TEXT,
    newText: text
});

export default messagesReducer;