import {combineReducers, createStore} from "redux";
import activityReducer from "./ActivityReducer";
import messagesReducer from "./MessagesReducer";
import menuReducer from "./MenuReducer";
import eventsReducer from "./EventsReducer";
import groupsReducer from "./GroupsReducer";
import userReducer from "./UserReducer";

let reducers = combineReducers({
        activityPage: activityReducer,
        messagesPage: messagesReducer,
        menu: menuReducer,
        eventsPage: eventsReducer,
        groupsPage: groupsReducer,
        user: userReducer
    }
);

let store = createStore(reducers);

window.store = store;

export default store;